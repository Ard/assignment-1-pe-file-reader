/// @file
/// @brief Implementation of PEFile.h

#include "PEFile.h"

/**
 * @brief Decodes write status
 * @param[in] status status of writing
 * @return message corresponding to the status
 */
char * write_status(const enum write_status status) {
    char * message[5] = {
        "Succesful write",
        "Write error: failed to move the pointer",
        "Write error: failed to allocate memory for section data",
        "Write error: failed to read section data",
        "Write error: failed to write section data"
    };
    if (status >=4) {
        return "write error";
    } else {
        return message[status];
    }
}


/**
 * @brief Decodes read status
 * @param[in] status status of reading
 * @return message corresponding to the status
 */
char * pe_read_status(const enum read_status status) {
    char * message[10] = {
        "Succesful read",
        "PE read error: file cannot be read",
        "PE read error: invalid signature offset",
        "PE read error: invalid signature",
        "PE read error: failed to move the pointer",
        "PE read error: failed to read magic number",
        "PE read error: the magic number is wrong",
        "PE read error: failed to read PE header",
        "PE read error: failed to allocate memory for section headers",
        "PE read error: failed to read the section headers"
    };
    if (status >= 9) {
        return "PE read error";
    } else {
        return message[status];
    }
}


/**
 * @brief Read PE file
 * @param[in] in input file
 * @param[in] PE_File structure describes PE file
 * @return status reading from file
 */
enum read_status from_pe(FILE* const in, struct PEFile *PE_File ) {
    if (in == NULL) {
        return FILE_READ_ERROR;
    }
    
    fseek(in, OFFSET, SEEK_SET);
    if (fread(&PE_File->offset, sizeof(uint32_t), 1, in) != 1){
        return PE_READ_INVALID_SIGNATURE_OFFSET;
    }
    if (fseek(in, PE_File->offset, SEEK_SET) != 0) {
        return PE_READ_INVALID_POINTER;
    };

    if (!fread(&PE_File->magic, sizeof(PE_File->magic), 1, in)) 
        return PE_READ_NO_MAGIC;

    if (PE_File->magic != MAGIC_VALUE) 
        return PE_READ_INVALID_MAGIC;  

    if (!fread(&PE_File->header, sizeof(PE_File->header), 1, in))
        return PE_READ_INVALID_HEADER;

    PE_File->section_headers = malloc(PE_File->header.number_of_sections * sizeof(struct SectionHeader));

    if (!PE_File->section_headers){
        return PE_READ_CANNOT_ALLOCATE_MEMORY;
    }
    
    fseek(in, PE_File->header.size_of_optional_header, SEEK_CUR);
    for (uint16_t i = 0; i < PE_File->header.number_of_sections; i++) {
        if (!fread(&PE_File->section_headers[i], sizeof(struct SectionHeader), 1, in))
            return PE_READ_CANNOT_READ_SECTION_HEADERS;    
    }

    return PE_READ_OK;
}

/**
 * @brief Find section by name
 * @param[in] PE_File structure describes PE file
 * @param[in] name name of the section to be found
 * @return The section header data structure or an empty section header
 */
struct SectionHeader *find_section_by_name(const struct PEFile* PE_File, const char* name) {
    for (uint16_t i = 0; i < PE_File->header.number_of_sections; i++) {
        if (strcmp(name, PE_File->section_headers[i].name) == 0)
            return PE_File->section_headers + i;
    }
    return NULL;
}

/**
 * @brief Write section data to file
 * @param[in] in input file
 * @param[in] out output file
 * @param[in] section SectiorHeader structure contains data
 * @return status writing to file
 */
enum write_status write_section(FILE *in, FILE *out, const struct SectionHeader *section) {
    if (fseek(in, section->pointer_to_raw_data, SEEK_SET) != 0) 
        return WRITE_INVALID_POINTER;

    char *data = malloc(section->size_of_raw_data);
    if (!data)
        return CANNOT_ALLOCATE_MEMORY_FOR_SECTION;

    if (!fread(data, section->size_of_raw_data, 1, in)){
        free(data);
        return CANNOT_READ_DATA;
    }

    if (!fwrite(data, section->size_of_raw_data, 1, out))
        return CANNOT_WRITE_DATA;
    free(data);

    return WRITE_OK;
}
