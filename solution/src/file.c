/// @file
/// @brief Implementation of file.h

#include "file.h"

/**
 * @brief Open the file
 * @param[in] file file pointer
 * @param[in] filename file name
 * @param[in] open_mode mode to open the file
 * @return 1 if file opened successfully
 */
bool open_file(FILE** file, char* const filename, char * open_mode) {
    *file = fopen(filename, open_mode);
    return (*file != NULL);
}

/**
 * @brief Close the file
 * @param[in] file file pointer
 * @return 1 if file closed successfully
 */
bool close_file(FILE* file) {
    return fclose(file) == 0;
}
