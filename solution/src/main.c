/// @file 
/// @brief Main application file

#include "file.h"
#include "PEFile.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f) {
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code 1
int main(int argc, char** argv) {
	if (argc != 4) {
    	usage(stdout);
    	return 1;
  	}

	struct PEFile PE_File;
  	FILE *in = NULL;
	FILE *out = NULL;

	if (!open_file(&in, argv[1], "rb+")) {
		printf("Can't open read file\n");
		return 1;
	}

	if (!open_file(&out, argv[3], "wb+")) {
    	close_file(in);
		printf("Can't open write file\n");
		return 1;
	}

  	const enum read_status statusFrom = from_pe(in, &PE_File);
 	if(statusFrom != 0) {
   		close_file(in);
		close_file(out);
		free(PE_File.section_headers);
		printf("%s\n", pe_read_status(statusFrom));
		return 1;
  	}

	struct SectionHeader *section = find_section_by_name(&PE_File, argv[2]);
	if (!section) {
		close_file(in);
		close_file(out);
		free(PE_File.section_headers);
		printf("No section with given name");
		return 1;
	}


  	const enum write_status statusTo = write_section(in, out, section);
	printf("%s\n", write_status(statusTo));
  	if(statusTo != 0) {
    	close_file(in);
		close_file(out);
		free(PE_File.section_headers);
		return 1;
  	}

	if (close_file(in) != 1) {
		printf("Can't close read file");
        return 1;
    }

    if (close_file(out) != 1) {
		printf("Can't close write file");
        return 1;
    }

	free(PE_File.section_headers);

	return 0;
}
