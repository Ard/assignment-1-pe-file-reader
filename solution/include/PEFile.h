/// @file
/// @brief File contains structures for PE file and functions working with it

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// File offset to the PE signature
#define OFFSET 0x3c

/// A 4-byte signature that identifies the file as a PE format image file
#define MAGIC_VALUE 0x00004550

/// Enum of file open statuses
enum read_status {
    PE_READ_OK = 0,
    FILE_READ_ERROR, 
    PE_READ_INVALID_SIGNATURE_OFFSET, 
    PE_READ_INVALID_SIGNATURE, 
    PE_READ_INVALID_POINTER, 
    PE_READ_NO_MAGIC, 
    PE_READ_INVALID_MAGIC, 
    PE_READ_INVALID_HEADER, 
    PE_READ_CANNOT_ALLOCATE_MEMORY, 
    PE_READ_CANNOT_READ_SECTION_HEADERS
};

/// Enum of file close statuses
enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_POINTER,
    CANNOT_ALLOCATE_MEMORY_FOR_SECTION, 
    CANNOT_READ_DATA, 
    CANNOT_WRITE_DATA
};

/**
 * @brief Structure contains PE header
 * 
 * @note This structure is packed for correct work with it.
 **/
#ifdef _MSC_VER
    #pragma pack(push, 1)
#endif
struct
#if defined __clang__ || defined __GNUC__
    __attribute__((packed))
#endif
PE_Header {
    /// The number that identifies the type of target machine
    uint16_t machine;

    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t number_of_sections;

    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    uint32_t time_date_stamp;

    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t pointer_to_symbol_table;

    /// The number of entries in the symbol table
    uint32_t number_of_symbols;

    /// The size of the optional header, which is required for executable files but not for object files
    uint16_t size_of_optional_header;

    /// The flags that indicate the attributes of the file
    uint16_t characteristics;
};
#ifdef _MSC_VER
    #pragma pack(pop)
#endif



/**
 * @brief The SectionHeader struct represents a section header in a PE file
 * Contains information about a specific section in the PE file
 * @note This structure is packed for correct work with it.
 */
#ifdef _MSC_VER
    #pragma pack(push, 1)
#endif
struct
#if defined __clang__ || defined __GNUC__
    __attribute__((packed))
#endif
SectionHeader {
    /// An 8-byte, null-padded UTF-8 encoded string
    char name[8];

    /// The total size of the section when loaded into memory
    uint32_t virtual_size;

    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory
    uint32_t virtual_address;

    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t size_of_raw_data;

    /// The file pointer to the first page of the section within the COFF file
    uint32_t pointer_to_raw_data;

    /// The file pointer to the beginning of relocation entries for the section
    uint32_t pointer_to_relocations;

    /// The file pointer to the beginning of line-number entries for the section
    uint32_t pointer_to_linenumbers;

    /// The number of relocation entries for the section
    uint16_t number_of_relocations;

    /// The number of line-number entries for the section
    uint16_t number_of_linenumbers;

    /// The flags that describe the characteristics of the section
    uint32_t characteristics;
};
#ifdef _MSC_VER
	#pragma pack(pop)
#endif


/**
 * @brief Structure describe all needed information about PE file
 */
struct PEFile {
    /// Magic number that identifies the file as a PE format image file
    uint32_t magic;
    /// Offset of the pointer to the main PE header
    uint32_t offset;
    /// Main PE file header
    struct PE_Header header;
    /// Array of section headers
    struct SectionHeader* section_headers;
};

/**
 * @brief Decodes write status
 * @param[in] status status of writing
 * @return message corresponding to the status
 */
char * write_status(const enum write_status status);

/**
 * @brief Decodes read status
 * @param[in] status status of reading
 * @return message corresponding to the status
 */
char * pe_read_status(const enum read_status status);

/**
 * @brief Read PE file
 * @param[in] in input file
 * @param[in] PE_File structure describes PE file
 * @return status reading from file
 */
enum read_status from_pe(FILE* const in, struct PEFile *PE_File);

/**
 * @brief Find section by name
 * @param[in] PE_File structure describes PE file
 * @param[in] name name of the section to be found
 * @return The section header data structure or an empty section header
 */
struct SectionHeader *find_section_by_name(const struct PEFile* PE_File, const char* name);

/**
 * @brief Write section data to file
 * @param[in] in input file
 * @param[in] out output file
 * @param[in] section SectiorHeader structure contains data
 * @return status writing to file
 */
enum write_status write_section(FILE *in, FILE *out, const struct SectionHeader *section);
