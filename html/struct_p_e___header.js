var struct_p_e___header =
[
    [ "characteristics", "struct_p_e___header.html#ac3a40ec2635d2ed88c67771221beeb6b", null ],
    [ "machine", "struct_p_e___header.html#ab1a8e54b8e602342d612aae9ee6d9879", null ],
    [ "number_of_sections", "struct_p_e___header.html#a6db1fca74ac37617782ce008e7b4d3a9", null ],
    [ "number_of_symbols", "struct_p_e___header.html#ab73d85a6edc2bb9e66578d874f32d338", null ],
    [ "pointer_to_symbol_table", "struct_p_e___header.html#ad950a8e552640331bcd9be11f4f7e422", null ],
    [ "size_of_optional_header", "struct_p_e___header.html#abb57c321b753db7551fe0ddc9fbd7555", null ],
    [ "time_date_stamp", "struct_p_e___header.html#a67c3aea07f08a2841dc822328b1593ac", null ]
];