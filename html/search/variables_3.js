var searchData=
[
  ['name_0',['name',['../struct_section_header.html#a1ee73c3ca64350aa94c52fc585918629',1,'SectionHeader']]],
  ['number_5fof_5flinenumbers_1',['number_of_linenumbers',['../struct_section_header.html#aef806b08901fc26956b890bd37ca19f9',1,'SectionHeader']]],
  ['number_5fof_5frelocations_2',['number_of_relocations',['../struct_section_header.html#ad4e354009d17a0e035aa8f86af1bdcf5',1,'SectionHeader']]],
  ['number_5fof_5fsections_3',['number_of_sections',['../struct_p_e___header.html#a6db1fca74ac37617782ce008e7b4d3a9',1,'PE_Header']]],
  ['number_5fof_5fsymbols_4',['number_of_symbols',['../struct_p_e___header.html#ab73d85a6edc2bb9e66578d874f32d338',1,'PE_Header']]]
];
