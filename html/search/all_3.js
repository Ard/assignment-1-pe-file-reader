var searchData=
[
  ['file_2ec_0',['file.c',['../file_8c.html',1,'']]],
  ['file_2eh_1',['file.h',['../file_8h.html',1,'']]],
  ['find_5fsection_5fby_5fname_2',['find_section_by_name',['../_p_e_file_8h.html#a6c35b322025ec572c1a27e4f8269d959',1,'find_section_by_name(const struct PEFile *PE_File, const char *name):&#160;PEFile.c'],['../_p_e_file_8c.html#a6c35b322025ec572c1a27e4f8269d959',1,'find_section_by_name(const struct PEFile *PE_File, const char *name):&#160;PEFile.c']]],
  ['from_5fpe_3',['from_pe',['../_p_e_file_8h.html#a637a3fd8c5044a8a97f925717352cd13',1,'from_pe(FILE *const in, struct PEFile *PE_File):&#160;PEFile.c'],['../_p_e_file_8c.html#a637a3fd8c5044a8a97f925717352cd13',1,'from_pe(FILE *const in, struct PEFile *PE_File):&#160;PEFile.c']]]
];
