var _p_e_file_8h =
[
    [ "PE_Header", "struct_p_e___header.html", "struct_p_e___header" ],
    [ "SectionHeader", "struct_section_header.html", "struct_section_header" ],
    [ "PEFile", "struct_p_e_file.html", "struct_p_e_file" ],
    [ "MAGIC_VALUE", "_p_e_file_8h.html#af6d3efc83e98e4e8f2da98b82a72a1e5", null ],
    [ "OFFSET", "_p_e_file_8h.html#a21005f9f4e2ce7597c5eb4351816d7e2", null ],
    [ "read_status", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303", [
      [ "PE_READ_OK", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303ab61e199a64d957afccbd243baed0172f", null ],
      [ "FILE_READ_ERROR", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a08e594c1afff0b7a6fc5a16b80801215", null ],
      [ "PE_READ_INVALID_SIGNATURE_OFFSET", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a8898a65c575f3739197a0923682944f1", null ],
      [ "PE_READ_INVALID_SIGNATURE", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303af6b389c9d3195d0d495e3a8828a1c1f7", null ],
      [ "PE_READ_INVALID_POINTER", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a00ecc8e682b6b404494375d1bacea03e", null ],
      [ "PE_READ_NO_MAGIC", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303ae9fcc262122c79a2282a450780777279", null ],
      [ "PE_READ_INVALID_MAGIC", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a518eecd4be750a0efc47dcb9106b7eac", null ],
      [ "PE_READ_INVALID_HEADER", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a74b88dd3e308db87862c82f305b9fa43", null ],
      [ "PE_READ_CANNOT_ALLOCATE_MEMORY", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a49558696e8f88aa70de9a075fed4f369", null ],
      [ "PE_READ_CANNOT_READ_SECTION_HEADERS", "_p_e_file_8h.html#a4d84c66cd3c22eb9c38633d500a86303a0b4cb3d16feee9e037e344437bac408d", null ]
    ] ],
    [ "write_status", "_p_e_file_8h.html#ae852c29c3f55482704e8b5d38cb07351", [
      [ "WRITE_OK", "_p_e_file_8h.html#ae852c29c3f55482704e8b5d38cb07351a2f895e17d9697fb39aece99ec0e9d496", null ],
      [ "WRITE_INVALID_POINTER", "_p_e_file_8h.html#ae852c29c3f55482704e8b5d38cb07351a007d5c5afe91c69cd6c746c8ac1d45d7", null ],
      [ "CANNOT_ALLOCATE_MEMORY_FOR_SECTION", "_p_e_file_8h.html#ae852c29c3f55482704e8b5d38cb07351a451639a5daa527755800bc73bbaefe71", null ],
      [ "CANNOT_READ_DATA", "_p_e_file_8h.html#ae852c29c3f55482704e8b5d38cb07351a763580e50353538e816738bf746b9e33", null ],
      [ "CANNOT_WRITE_DATA", "_p_e_file_8h.html#ae852c29c3f55482704e8b5d38cb07351ae8e9782279bc9e4e45ec1b6c8e9ff611", null ]
    ] ],
    [ "find_section_by_name", "_p_e_file_8h.html#a6c35b322025ec572c1a27e4f8269d959", null ],
    [ "from_pe", "_p_e_file_8h.html#a637a3fd8c5044a8a97f925717352cd13", null ],
    [ "pe_read_status", "_p_e_file_8h.html#a49c6c4ca6c6b3a28cbe6085f3904847b", null ],
    [ "write_section", "_p_e_file_8h.html#a0ad8934e5a72eb94ab2157bdb53547d4", null ],
    [ "write_status", "_p_e_file_8h.html#a2a19f6a3480bb9f1b1810f6954d5d1ea", null ]
];