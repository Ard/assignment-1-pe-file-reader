var struct_section_header =
[
    [ "characteristics", "struct_section_header.html#ace6333402c70fd34d1091da803e4d7f4", null ],
    [ "name", "struct_section_header.html#a1ee73c3ca64350aa94c52fc585918629", null ],
    [ "number_of_linenumbers", "struct_section_header.html#aef806b08901fc26956b890bd37ca19f9", null ],
    [ "number_of_relocations", "struct_section_header.html#ad4e354009d17a0e035aa8f86af1bdcf5", null ],
    [ "pointer_to_linenumbers", "struct_section_header.html#a76d3e60e9c3a7a188ce96557afb77483", null ],
    [ "pointer_to_raw_data", "struct_section_header.html#a5827e5ec05f18463d67f9d87039a92d3", null ],
    [ "pointer_to_relocations", "struct_section_header.html#ad637930a56c9b6d609d807379cf36799", null ],
    [ "size_of_raw_data", "struct_section_header.html#a706540c08f963e2ac95570a9695cbc14", null ],
    [ "virtual_address", "struct_section_header.html#aa78568fb4ba73a44082cf5ae8d56ba78", null ],
    [ "virtual_size", "struct_section_header.html#adf27aaee76b6c4bc2b09398ae8d1d279", null ]
];